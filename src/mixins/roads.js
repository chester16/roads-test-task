export const roads = {
    data() {
        return {
            baseUrl: 'https://test-task.lexfoxer.now.sh/api/roads.js?'
        }
    },
    methods: {
        loadRoads(parameters = ''){
            window.axios.get(this.baseUrl + '&' + parameters)
                .then(response => {
                    this.$store.commit('roads', response.data.data);
                })
        }
    }

};
