import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import { store } from './store/store'

Vue.config.productionTip = false;

window.axios = axios;

new Vue({
  render: h => h(App),
  store
}).$mount('#app');
