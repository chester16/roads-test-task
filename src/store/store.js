import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        roads: []
    },
    mutations: {
        roads(state, roads){
            state.roads = [...roads];
        }
    },
    getters:{
        roads: state => state.roads
    }
});

