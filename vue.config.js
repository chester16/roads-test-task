module.exports = {
    devServer: {
        host: 'localhost',
        port: '8080'
    },
    css: {
        modules: true,
        loaderOptions: {
            sass:{
                data: `
                @import "~bootstrap/scss/bootstrap";
                @import "~bootstrap/scss/mixins";
                @import "~bootstrap/scss/variables";
                `
            }
        }
    }
};
